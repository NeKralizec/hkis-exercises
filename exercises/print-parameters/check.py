import re
import sys
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    solution = Path("solution.py").read_text()
    if re.match("[^#]*__file__", solution, re.M):
        checker.fail("You don't need __file__, you need sys.argv.")
    if output.endswith("solution.py"):
        sys.exit(0)  # OK :)
    if "[" in output and "]" in output:
        checker.fail(
            "Looks like you printed a list, I just need the program name. Got:",
            checker.code(output),
        )
    checker.fail("Expected to see the program name, got:", checker.code(output))


if __name__ == "__main__":
    check()
