from gettext import gettext, textdomain
from pathlib import Path
from time import perf_counter

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
textdomain("hkis")

with checker.student_code():
    from solution import is_a_dyck_word


def check_parenthesis(opening=None, closing=None):
    tests = [
        ("", True),
        ("()", True),
        ("(((", False),
        ("((()", False),
        ("()))", False),
        ("()))((()", False),
        ("()()()(", False),
        ("(((())))", True),
        ("((()()))", True),
        ("(()(()))", True),
        ("(()()())", True),
        ("((())())", True),
        ("()((()))", True),
        ("()(()())", True),
        ("()()(())", True),
        ("()()()()", True),
        ("()(())()", True),
        ("(())(())", True),
        ("(())()()", True),
        ("((()))()", True),
        ("(()())()", True),
        ("f(((())))", False),
        ("(f(()()))", False),
        ("((f)(()))", False),
        ("(()f()())", False),
        ("((()f)())", False),
        ("()(((f)))", False),
    ]
    for word, expected in tests:
        if opening and closing:
            word = (
                word.replace(")", "\x00").replace("(", opening).replace("\x00", closing)
            )
        with checker.student_code():
            theirs = is_a_dyck_word(word)
        assert theirs is expected, (word, expected)


def main():
    try:
        check_parenthesis()
    except AssertionError as err:
        word, result = err.args[0]
        checker.fail(
            gettext(
                "Wrong result for `is_a_dyck_word({word})`, expected: `{expected}`, got: `{got}`."
            ).format(word=repr(word), expected=result, got=not result)
        )

    try:
        check_parenthesis("<", ">")
        check_parenthesis("«", "»")
        check_parenthesis("[", "]")
        check_parenthesis("→", "←")
        check_parenthesis("「", "」")
        check_parenthesis("{", "}")
        check_parenthesis("☹", "☺")
    except AssertionError as err:
        word, result = err.args[0]
        checker.fail(
            gettext(
                "You're successfully passing tests with parenthesis! Let's run more tests with some other characters now..."
            ),
            gettext(
                "Wrong result for `is_a_dyck_word({word})`, expected: `{expected}`, got: `{got}`."
            ).format(word=repr(word), expected=result, got=not result),
        )


if __name__ == "__main__":
    main()
