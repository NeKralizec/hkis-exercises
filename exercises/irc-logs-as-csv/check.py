import os
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
Path("francejs.csv").write_text(
    """1,1380612632141,"mdk","oa: Hello :)"
1,1380612679888,"oa","mdk: salut"
4,1380612632141,"mdk2","oa2: Hello :)"
4,1380612679888,"oa2","mdk2: salut"
4,1380612632141,"mdk2","oa2: Hello :)"
4,1380612679888,"oa2","mdk2: salut"
"""
)


def validate_format(output):
    if ", " not in output:
        checker.fail(
            f"""You're expected to write two logins, separated by a coma and a space.
I don't find the coma-space thing in your output, you printed:

{checker.code(output)}
"""
        )
    if "\n" in output:
        checker.fail(
            f"""You're expected to write two logins, separated by a coma and a space.
I see multiple line, while it should fit on a single line, you printed:

{checker.code(output)}
"""
        )


def check_no_leave():
    with open("francejs.csv", "w") as francejs:
        francejs.write("""1,1380612632141,"mdk","oa: Hello :)"\n""")
        francejs.write("""1,1380612679888,"oa","mdk: salut"\n""")
        francejs.write("""4,1380612632141,"ada","alan: Hello :)"\n""")
        francejs.write("""4,1380612679888,"alan","ada: salut"\n""")
        francejs.write("""4,1380612632141,"ada","alan: Hello :)"\n""")
        francejs.write("""4,1380612679888,"alan","ada: salut"\n""")

    output = checker.run("solution.py")
    validate_format(output)
    if output == "ada, alan" or output == "alan, ada":
        checker.fail(
            """The first column indicates the message type.
Leaving the chan is not speaking.
"""
        )


def check_one_clique():
    with open("francejs.csv", "w") as francejs:
        francejs.write("""1,1380612632141,"mdk","oa: Hello :)"\n""")
        francejs.write("""1,1380612679888,"oa","mdk: salut"\n""")

    output = checker.run("solution.py")
    validate_format(output)
    if output != "mdk, oa" and output != "oa, mdk":
        checker.fail(
            f"""For a very simple test with only two lines, it does not work.
Expected:

    mdk, oa

(or `oa, mdk`, I don't care about the order)

Got:

{checker.code(output)}
"""
        )


def check_two_cliques():
    with open("francejs.csv", "w") as francejs:
        francejs.write("""1,1380612632141,"mdk","oa: Hello :)"\n""")
        francejs.write("""1,1380612679888,"oa","mdk: salut"\n""")
        francejs.write("""1,1380612632141,"ada","alan: Hello :)"\n""")
        francejs.write("""1,1380612679888,"alan","ada: salut"\n""")
        francejs.write("""1,1380612632141,"ada","alan: Hello :)"\n""")
        francejs.write("""1,1380612679888,"alan","ada: salut"\n""")

    output = checker.run("solution.py")
    validate_format(output)
    if output == "mdk, oa" or output == "oa, mdk":
        checker.fail("I found a group speaking more than this one.")


def check_spaces():
    with open("francejs.csv", "w") as francejs:
        francejs.write("""1,1380612632141,"mdk","oa: Hello :)"\n""")
        francejs.write("""1,1380612679888,"oa","mdk: salut"\n""")
        francejs.write("""1,1380612632141,"ada","alan : Hello :)"\n""")
        francejs.write("""1,1380612679888,"alan","ada : salut"\n""")
        francejs.write("""1,1380612632141,"ada","alan : Hello :)"\n""")
        francejs.write("""1,1380612679888,"alan","ada : salut"\n""")

    output = checker.run("solution.py")
    validate_format(output)
    if output == "mdk, oa" or output == "oa, mdk":
        checker.fail(
            "Some are putting spaces before the column when highlighing someone :("
        )


def main():
    try:
        check_no_leave()
        check_one_clique()
        check_two_cliques()
        check_spaces()
    finally:
        try:
            os.unlink("francejs.csv")
        except Exception:
            pass


if __name__ == "__main__":
    main()
