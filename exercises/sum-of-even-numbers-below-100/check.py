from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")


def check():
    output = checker.run("solution.py")
    trailer = ""
    if "def " in Path("solution.py").read_text(encoding="UTF-8"):
        trailer = (
            "Or maybe you defined your own function but forgot to call it?"
            "\n\n"
            "In this exercise, I'm just executing your script, "
            "I'm not calling your function."
        )
    if not output:
        checker.fail(
            """Your code printed nothing, did you forgot to call the
[print](https://docs.python.org/3/library/functions.html#print) function?""",
            trailer,
        )
    solution = Path("solution.py").read_text()
    try:
        output = int(output)
    except ValueError:
        try:
            integers = [int(x) for x in output.split("\n")]
            checker.fail(
                f"""I expect your output to be an integer,
got {len(integers)} numbers instead:""",
                checker.code(output),
            )
        except ValueError:
            checker.fail(
                "I expect your output to be an integer (the sum), got:",
                checker.code(output),
            )
    if output == 100 and "=+" in solution:
        checker.fail(
            """Did you mistyped `+=` as `=+`?

```python
value =+ 10  # is equivalent to:
value = (+ 10)  # which is equivalent to:
value = 10
```

in the other hand:

```python
value += 10  # is a shortcut to:
value = value + 10
```
"""
        )

    if output == sum(i for i in range(103) if i % 2 == 0):
        checker.fail("Looks like you counted 102, which is greater than 100.")
    if output == sum(i for i in range(100) if i % 2 == 0):
        checker.fail(
            """I'm asking `<= 100`, not `< 100`.
Beware, in Python, ranges are semi-open: start is included, stop is not."""
        )
    if output == sum(i for i in range(100) if i % 2):
        checker.fail("This is the sum of odd number < 100 :(")
    if output == sum(i for i in range(101) if i % 2):
        checker.fail("This is the sum of odd number :(")
    if output == sum(range(100)):
        checker.fail(
            """This is the sum of natural numbers < 100,
I need only even numbers (divisible by two)."""
        )
    if output == sum(range(101)):
        checker.fail(
            """This is the sum of natural numbers <= 100,
I need only even numbers (divisible by two)."""
        )
    if output == sum(i for i in range(101) if i % 2 == 0):
        return  # Success!

    if output < 100:
        checker.fail(
            "You gave:",
            checker.code(output),
            """which looks low. There's 98 and 96 that are both even numbers below 100
and whose sum is greater than 100.""",
        )

    checker.fail(
        f"I don't get the same sum as you, FYI you gave:\n\n{checker.code(output)}"
    )


if __name__ == "__main__":
    check()
