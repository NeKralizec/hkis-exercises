from gettext import gettext, textdomain
import sys
from pathlib import Path

import correction_helper as checker

checker.exclude_file_from_traceback(__file__)
Path("solution").rename("solution.py")
textdomain("hkis")


def check():
    with open("solution.py") as f:
        solution = "\n".join(
            line for line in f.read().lower().split("\n") if not line.startswith("#")
        )
    if not solution:
        checker.fail(
            gettext(
                """You wrote no code (in the black area, which is a code editor).

You should write a line of code which prints `Hello world!` when
executed, if you really don't know how to start, you should first read
the [tutorial](https://docs.python.org/3/tutorial/)."""
            )
        )
    if solution.strip().replace("!", "") == "hello world":
        checker.fail(
            gettext(
                """You need to use the
[print](https://docs.python.org/3/library/functions.html#print) function
in order to make Python print something."""
            )
        )
    if solution.strip().replace("!", "") == "print(hello world)":
        checker.fail(
            gettext(
                """"Hello world!" is a string, in Python strings should be enclosed
 in quotes or double quotes.
See the [strings tutorial](
https://docs.python.org/3/tutorial/introduction.html#strings)"""
            )
        )
    output = checker.run("solution.py")
    clean_output = output.lower().replace(",", "").replace("!", "")
    if "hello world" in clean_output:
        print(
            gettext("Yep! This code prints:"),
            checker.code(output),
            gettext("You can try the next exercise by clicking the blue button below."),
            sep="\n\n",
        )
        sys.exit(0)
    if "print" not in solution and "hello" in solution:
        checker.fail(
            gettext(
                """You're not in an
[interactive Python interpreter](
https://docs.python.org/3/tutorial/interpreter.html#interactive-mode),
your code is tested in a file,
so there is no implicit print here.

You have to use the [print](https://docs.python.org/3/library/functions.html#print)
function."""
            )
        )
    message = gettext(
        'You should print "Hello World!", not something else to validate this exercise.'
    )

    if not output:
        checker.fail(message + "\n\n" + gettext("You printed nothing :("))
    else:
        checker.fail(message, gettext("You printed:"), checker.code(output))


if __name__ == "__main__":
    check()
