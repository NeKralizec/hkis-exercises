Write a function named `battery_charge` that graphically represents
a battery’s charge. Your function doesn't need to return anything,
but just show how much a battery is charged.

This function will takes an `int` between `0` and `100` as a
parameter, display a bar to represent how much the battery is "filled"
(from `0` to `10` bars), then prints the percentage.

The number of bars should be
[rounded](https://docs.python.org/3/library/functions.html#round),
meaning that both 9% and 11% are represented by one bar.


## Examples

```
>>> battery_charge(0)
[          ]
0%
>>> battery_charge(5)
[          ]
5%
>>> battery_charge(9)
[❚         ]
9%
>>> battery_charge(11)
[❚         ]
11%
>>> battery_charge(100)
[❚❚❚❚❚❚❚❚❚❚]
100%
```

If the `❚` character causes you problems in your editor or terminal, feel free to
use a simple `|` instead.


## References

- The [round](https://docs.python.org/3/library/functions.html#round) builtin function.
